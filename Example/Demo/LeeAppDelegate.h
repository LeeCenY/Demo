//
//  LeeAppDelegate.h
//  Demo
//
//  Created by LeeCenY on 06/14/2017.
//  Copyright (c) 2017 LeeCenY. All rights reserved.
//

@import UIKit;

@interface LeeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
