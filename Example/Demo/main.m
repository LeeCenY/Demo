//
//  main.m
//  Demo
//
//  Created by LeeCenY on 06/14/2017.
//  Copyright (c) 2017 LeeCenY. All rights reserved.
//

@import UIKit;
#import "LeeAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LeeAppDelegate class]));
    }
}
