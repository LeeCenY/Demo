//
//  UIViewController+Utility.h
//  OpeningAccount
//
//  Created by RyanLee on 3/16/17.
//  Copyright © 2017 TTL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utility)

- (void)setNavigationBarStyle;

@end
