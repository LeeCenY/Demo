//
//  UIViewController+Utility.m
//  OpeningAccount
//
//  Created by RyanLee on 3/16/17.
//  Copyright © 2017 TTL. All rights reserved.
//

#import "UIViewController+Utility.h"

@implementation UIViewController (Utility)

- (void)setNavigationBarStyle {
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    
    if ([NSStringFromClass([self class]) isEqualToString:@"HomePageViewController"] || [NSStringFromClass([self class]) isEqualToString:@"StartRegisterViewController"]) {
        UIImageView *companyLogoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, navigationBar.frame.size.width, navigationBar.frame.size.height - 10)];
        companyLogoImageView.image = [UIImage imageNamed:@"thomaslogo"];
        companyLogoImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.navigationItem.titleView = companyLogoImageView;
        
        [navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        navigationBar.titleTextAttributes = @{ NSForegroundColorAttributeName:[UIColor whiteColor] };
        
        self.title = @"";
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        [navigationBar setBackgroundImage:[UIImage imageNamed:@"title_bg"] forBarMetrics:UIBarMetricsDefault];
        
        self.title = @"石匠环球金服线上开户";
    }
}

@end
