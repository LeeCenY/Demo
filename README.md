# Demo

[![CI Status](http://img.shields.io/travis/LeeCenY/Demo.svg?style=flat)](https://travis-ci.org/LeeCenY/Demo)
[![Version](https://img.shields.io/cocoapods/v/Demo.svg?style=flat)](http://cocoapods.org/pods/Demo)
[![License](https://img.shields.io/cocoapods/l/Demo.svg?style=flat)](http://cocoapods.org/pods/Demo)
[![Platform](https://img.shields.io/cocoapods/p/Demo.svg?style=flat)](http://cocoapods.org/pods/Demo)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Demo is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Demo"
```

## Author

LeeCenY, 634348197@qq.com

## License

Demo is available under the MIT license. See the LICENSE file for more info.
